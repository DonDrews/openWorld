void AdjustImageLevelsAlpha(Image& image, float factor)
{
  const unsigned numLevels = GetNumImageLevels(image);
  if (numLevels <= 1)
  {
    return;
  }

  SharedPtr<Image> level = image.GetNextLevel();
  float k = factor;
  for (unsigned i = 1; i < numLevels; ++i)
  {
    for (int y = 0; y < level->GetHeight(); ++y)
    {
      for (int x = 0; x < level->GetWidth(); ++x)
      {
        Color color = level->GetPixel(x, y);
        color.a_ *= k;
        level->SetPixel(x, y, color);
      }
    }
    k *= factor;
    level = level->GetNextLevel();
  }
}

int main(char** argc, int argv)
{
  if(strcmp(argc[1], "mipimage") == 0 && argv == 4)
  {
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    //load image
    Image toBeChanged = cache->GetResource<Image>(argc[2]);

    float mipFactor = atof(argc[3]);

    //change mips
    AdjustImageLevelsAlpha(*toBeChanged, mipFactor);

    //save image again
  }
}
