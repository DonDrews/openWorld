#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Resource/XMLFile.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/DebugRenderer.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Skybox.h>
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/UI/Cursor.h>
#include <Urho3D/Engine/DebugHud.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/View.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>
#include <Urho3D/UI/Sprite.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Core/Timer.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/Resource/XMLFile.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/IO/PackageFile.h>
#include <Urho3D/Physics/PhysicsEvents.h>
#include <Urho3D/Physics/PhysicsWorld.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Input/Controls.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Network/Connection.h>
#include <Urho3D/Network/Network.h>
#include <Urho3D/Network/NetworkEvents.h>
#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Physics/PhysicsEvents.h>
#include <Urho3D/Physics/PhysicsWorld.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/LineEdit.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/Graphics/RenderPath.h>
#include <iostream>

#include "PlanetArea.h"
#include "Player.h"
#include "ProcSky.h"
#include "MainMenu.h"
#include "PlanetLoadMenu.h"

using namespace Urho3D;
using namespace std;

bool remoteContentSetup = false;

/// Scene.
SharedPtr<Scene> scene_;
/// Camera scene node.
SharedPtr<Node> cameraNode_;
/// Camera yaw angle.
float yaw_;
/// Camera pitch angle.
float pitch_;
// Identifier for our custom remote event we use to tell the client which object they control
static const StringHash E_CLIENTOBJECTID("ClientObjectID");
// Identifier for the node ID parameter in the event data
static const StringHash P_ID("ID");
/// ID of own controllable object (client only.)
unsigned clientObjectID_;
//headnode
Node* headNode_;

class OpenClient : public Application
{
public:

  OpenClient(Context* context) : Application(context)
  {}

  virtual void Setup()
  {
	OpenConsoleWindow();
    engineParameters_["WindowTitle"] = "Open World Client";
	engineParameters_["WindowIcon"] = "../../bin/Data/Textures/Icon.png";
    engineParameters_["FullScreen"]  = true;
	engineParameters_["WindowResizable"] = true;
    engineParameters_["VSync"] = false;
    engineParameters_["ResourcePrefixPaths"] = "../../bin/";

  }

  void MoveCamera()
  {
    // Right mouse button controls mouse cursor visibility: hide when pressed
    UI* ui = GetSubsystem<UI>();
    Cursor* c = ui->GetCursor();
    //bool test = c->IsVisible();
    Input* input = GetSubsystem<Input>();
    //ui->GetCursor()->SetVisible(false);

    if (input->GetKeyPress(KEY_ESCAPE))
    {
      Network* network = GetSubsystem<Network>();
      network->Disconnect();
      exit(-1);
    }

    // Mouse sensitivity as degrees per pixel
    const float MOUSE_SENSITIVITY = 0.1f;

    // Use this frame's mouse motion to adjust camera node yaw and pitch. Clamp the pitch and only move the camera
    // when the cursor is hidden
    if (!ui->GetCursor()->IsVisible())
    {
      IntVector2 mouseMove = input->GetMouseMove();
      yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
      pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
      pitch_ = Clamp(pitch_, -75.0f, 75.0f);
    }

    // Construct new orientation for the camera scene node from yaw and pitch. Roll is fixed to zero
    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    // Only move the camera / show instructions if we have a controllable object
    bool showInstructions = false;
    if (clientObjectID_)
    {
      Node* ballNode = scene_->GetNode(clientObjectID_);

      if (ballNode)
      {
  		  if (!headNode_)
  		  {
  			  headNode_ = ballNode->GetComponent<AnimatedModel>()->GetSkeleton().GetBone("head")->node_;
  		  }
        // Move camera some distance away from the ball
        //cameraNode_->SetPosition((headNode_->GetWorldPosition() + Vector3(0.0, 0.05, 0.0)) + cameraNode_->GetRotation() * Vector3(0.0, 0.1, 0.0));
        cameraNode_->SetPosition((ballNode->GetPosition() + Vector3(0.0, 1.7, 0.0)) + cameraNode_->GetRotation() * /*Vector3(-1.0, 2.0, -5.0)*/ Vector3(0.5, 0.1, -1.5));
        showInstructions = true;
      }
    }
  }

  void HandlePostUpdate(StringHash eventType, VariantMap& eventData)
  {
    // We only rotate the camera according to mouse movement since last frame, so do not need the time step
    MoveCamera();
  }

  void HandlePhysicsPreStep(StringHash eventType, VariantMap& eventData)
  {
    Renderer* renderer = GetSubsystem<Renderer>();
    // This function is different on the client and server. The client collects controls (WASD controls + yaw angle)
    // and sets them to its server connection object, so that they will be sent to the server automatically at a
    // fixed rate, by default 30 FPS. The server will actually apply the controls (authoritative simulation.)
    Network* network = GetSubsystem<Network>();
    Connection* serverConnection = network->GetServerConnection();
    UI* ui = GetSubsystem<UI>();
    Input* input = GetSubsystem<Input>();
    Controls controls;

    //initialize ProcSky render
    if(!remoteContentSetup)
    {
      Node* procNode = scene_->GetChild("ProcSky");
      if(procNode)
      {
        ProcSky* proc = procNode->GetComponent<ProcSky>();
        if(proc)
        {
          //pass in zone pointer so that the procsky texture can be assigned to the zone
          proc->Initialize(scene_->GetChild("Zone")->GetComponent<Zone>());
          remoteContentSetup = true;
        }
        else
        {
          URHO3D_LOGERROR("Unable to get ProcSky component");
        }
      }
      else
      {
        //URHO3D_LOGERROR("Unable to get ProcSky node");
      }
    }

    if (input->GetKeyPress(KEY_F5))
    {
        File saveFile(context_, GetSubsystem<FileSystem>()->GetProgramDir() + "CharacterDemo2.xml", FILE_WRITE);
        scene_->SaveXML(saveFile);
    }

    // Toggle debug HUD with F2
    if (input->GetKeyPress(KEY_F2))
      GetSubsystem<DebugHud>()->ToggleAll();

    if(input->GetKeyPress(KEY_F4))
    {
      UI* ui = GetSubsystem<UI>();
      ui->GetCursor()->SetVisible(!ui->GetCursor()->IsVisible());
    }

    // Copy mouse yaw
    controls.yaw_ = yaw_;

    // Only apply WASD controls if there is no focused UI element
    if (!ui->GetFocusElement())
    {
      controls.Set(CTRL_FORWARD, input->GetKeyDown(KEY_W));
      controls.Set(CTRL_BACK, input->GetKeyDown(KEY_S));
      controls.Set(CTRL_LEFT, input->GetKeyDown(KEY_A));
      controls.Set(CTRL_RIGHT, input->GetKeyDown(KEY_D));
      controls.Set(CTRL_JUMP, input->GetKeyDown(KEY_SPACE));
      controls.Set(CTRL_SPRINT, input->GetKeyDown(KEY_SHIFT));
      controls.Set(CTRL_CROUCH, input->GetKeyDown(KEY_CTRL));
      controls.Set(CTRL_TIME_UP, input->GetKeyDown(KEY_UP));
      controls.Set(CTRL_TIME_DOWN, input->GetKeyDown(KEY_DOWN));
      controls.Set(CTRL_WIELD, input->GetMouseButtonDown(MOUSEB_RIGHT));
    }

    if(serverConnection) //if connected already
    {
      serverConnection->SetControls(controls);
      // In case the server wants to do position-based interest management using the NetworkPriority components, we should also
      // tell it our observer (camera) position. In this sample it is not in use, but eg. the NinjaSnowWar game uses it
      serverConnection->SetPosition(cameraNode_->GetPosition());
    }
  }

  void HandleClientObjectID(StringHash eventType, VariantMap& eventData)
  {
      std::cout << "ID sent" << std::endl;
      clientObjectID_ = eventData[P_ID].GetUInt();
  }

  virtual void Start()
  {
    //Register custom classes
    PlanetArea::RegisterObject(context_);
    Imposter::RegisterObject(context_);
    ProcSky::RegisterObject(context_);
    Player::RegisterObject(context_);
    MainMenu::RegisterObject(context_);
    PlanetLoadMenu::RegisterObject(context_);

    scene_ = new Scene(context_);

    // Create scene content on the server only
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    // Create debug HUD.
    DebugHud* debugHud = engine_->CreateDebugHud();
    debugHud->SetDefaultStyle(cache->GetResource<XMLFile>("UI/DefaultStyle.xml"));

    // Create octree and physics world with default settings. Create them as local so that they are not needlessly replicated
    // when a client connects
    scene_->CreateComponent<Octree>(LOCAL);
    scene_->CreateComponent<PhysicsWorld>(LOCAL);
    // Create the camera. Limit far clip distance to match the fog
    // The camera needs to be created into a local node so that each client can retain its own camera, that is unaffected by
    // network messages. Furthermore, because the client removes all replicated scene nodes when connecting to a server scene,
    // the screen would become blank if the camera node was replicated (as only the locally created camera is assigned to a
    // viewport in SetupViewports() below)
    cameraNode_ = scene_->CreateChild("Camera", LOCAL);
    Camera* camera = cameraNode_->CreateComponent<Camera>();
	  camera->SetNearClip(0.15f);
	  camera->SetFarClip(10000.0f);
    camera->SetFov(90);

    // Set an initial position for the camera scene node above the plane
    cameraNode_->SetPosition(Vector3(0.0f, 5.0f, 0.0f));

    Network* network = GetSubsystem<Network>();
    String address = "localhost";

    SubscribeToEvents();

    UI* ui = GetSubsystem<UI>();
    UIElement* root = ui->GetRoot();
    XMLFile* uiStyle = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");
    // Set style to the UI root so that elements will inherit it
    root->SetDefaultStyle(uiStyle);

    // Create a Cursor UI element because we want to be able to hide and show it at will. When hidden, the mouse cursor will
    // control the camera, and when visible, it can interact with the login UI
    SharedPtr<Cursor> cursor(new Cursor(context_));
    cursor->SetStyleAuto(uiStyle);
    ui->SetCursor(cursor);
    // Set starting position of the cursor at the rendering window center
    Graphics* graphics = GetSubsystem<Graphics>();
    cursor->SetPosition(graphics->GetWidth() / 2, graphics->GetHeight() / 2);

    SetupViewport();

    // Connect to server, specify scene to use as a client for replication
    clientObjectID_ = 0; // Reset own object ID from possible previous connection

    //start menu to wait for connect
    MainMenu* menu = scene_->CreateComponent<MainMenu>(LOCAL);
    menu->SetScene(scene_);
  }

  void SetupViewport()
  {
    Renderer* renderer = GetSubsystem<Renderer>();

    ResourceCache* cache = GetSubsystem<ResourceCache>();

    //set HDR
    renderer->SetHDRRendering(true);

    //test
    renderer->SetDrawShadows(true);
    //renderer->SetShadowQuality(SHADOWQUALITY_BLUR_VSM);
    renderer->SetShadowMapSize(4096);

    // Set up a viewport to the Renderer subsystem so that the 3D scene can be seen
    RenderPath* path = new RenderPath();
    path->Load(cache->GetResource<XMLFile>("RenderPaths/ForwardHWDepth.xml"));
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>(), path));

    SharedPtr<RenderPath> effectRenderPath = viewport->GetRenderPath()->Clone();
    //effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/ssao.xml"));
    effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/AutoExposure.xml"));
    effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/Tonemap.xml"));
    effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/FXAA3.xml"));

    //effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/GammaCorrection.xml"));
    // Make the bloom mixing parameter more pronounced
    //effectRenderPath->SetEnabled("GammaCorrection", true);
    //effectRenderPath->SetEnabled("AutoExposure", true);
    //effectRenderPath->SetEnabled("Tonemap", true);
    viewport->SetRenderPath(effectRenderPath);

    renderer->SetViewport(0, viewport);
  }

  void SubscribeToEvents()
  {
    // Subscribe to fixed timestep physics updates for setting or applying controls
    SubscribeToEvent(E_PHYSICSPRESTEP, URHO3D_HANDLER(OpenClient, HandlePhysicsPreStep));

    // Subscribe HandlePostUpdate() method for processing update events. Subscribe to PostUpdate instead
    // of the usual Update so that physics simulation has already proceeded for the frame, and can
    // accurately follow the object with the camera
    SubscribeToEvent(E_POSTUPDATE, URHO3D_HANDLER(OpenClient, HandlePostUpdate));

    // This is a custom event, sent from the server to the client. It tells the node ID of the object the client should control
    SubscribeToEvent(E_CLIENTOBJECTID, URHO3D_HANDLER(OpenClient, HandleClientObjectID));
    // Events sent between client & server (remote events) must be explicitly registered or else they are not allowed to be received
    GetSubsystem<Network>()->RegisterRemoteEvent(E_CLIENTOBJECTID);
  }
};

URHO3D_DEFINE_APPLICATION_MAIN(OpenClient)
