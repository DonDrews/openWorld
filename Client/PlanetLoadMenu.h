#pragma once

#include "Menu.h"
#include "Imposter.h"
#include <iostream>

class PlanetLoadMenu : public Menu
{
public:

  URHO3D_OBJECT(PlanetLoadMenu, Menu);

  static void RegisterObject(Urho3D::Context* c);

  PlanetLoadMenu(Urho3D::Context* c);

  void HandleUpdate(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);

  bool pastFirstUpdate;
  Imposter::Stage currentStage;

private:

  Urho3D::Context* contextRef;
  Urho3D::ProgressBar* progressRef;
};
