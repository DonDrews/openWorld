#include "PlanetLoadMenu.h"

using namespace Urho3D;

void PlanetLoadMenu::RegisterObject(Context* c)
{
  c->RegisterFactory<PlanetLoadMenu>();
}

PlanetLoadMenu::PlanetLoadMenu(Context* c) : Menu(c)
{
  //load and instantiate menu from file
  ResourceCache* cache = GetSubsystem<ResourceCache>();
  UI* ui = GetSubsystem<UI>();
  ui->GetRoot()->SetDefaultStyle(cache->GetResource<XMLFile>("UI/DefaultStyle.xml"));
  ui->GetRoot()->LoadChildXML(cache->GetResource<XMLFile>("UI/PlanetLoadMenu.xml")->GetRoot());

  //save cache of progress bar
  progressRef = ui->GetRoot()->GetChildStaticCast<ProgressBar>("LoadProgress", true);

  SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(PlanetLoadMenu, HandleUpdate));

  contextRef = c;

  currentStage = Imposter::IMPOSTER_SETUP;
}

void PlanetLoadMenu::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
  Network* network = GetSubsystem<Network>();
  progressRef->SetValue(network->GetServerConnection()->GetDownloadProgress());
  if(pastFirstUpdate && network->GetServerConnection()->IsSceneLoaded())
  {
    if(currentStage == Imposter::IMPOSTER_SETUP)
    {
      //load packaged content
      ResourceCache* cache = GetSubsystem<ResourceCache>();
      SharedPtr<File> localCont = cache->GetFile("local", true);
      if(!localCont)
      {
        return; //not loaded yet
      }

      scene_->Instantiate(*localCont.Get(), Vector3(0, 0, 0), Quaternion(), LOCAL);

      //generate textures for imposters
      PODVector<Node*> imposters;
      scene_->GetChild("local")->GetChildrenWithComponent<Imposter>(imposters);
      for(int i = 0; i < imposters.Size(); i++)
      {
        imposters[i]->GetComponent<Imposter>()->RenderTextures(contextRef, currentStage);
      }

      currentStage = Imposter::IMPOSTER_FIRST;
    }
    else if(currentStage == Imposter::IMPOSTER_FIRST)
    {
      //generate textures for imposters
      PODVector<Node*> imposters;
      scene_->GetChild("local")->GetChildrenWithComponent<Imposter>(imposters);
      for(int i = 0; i < imposters.Size(); i++)
      {
        imposters[i]->GetComponent<Imposter>()->RenderTextures(contextRef, currentStage);
      }

      currentStage = Imposter::IMPOSTER_SECOND;
    }
    else if(currentStage == Imposter::IMPOSTER_SECOND)
    {
      //generate textures for imposters
      PODVector<Node*> imposters;
      scene_->GetChild("local")->GetChildrenWithComponent<Imposter>(imposters);
      for(int i = 0; i < imposters.Size(); i++)
      {
        imposters[i]->GetComponent<Imposter>()->RenderTextures(contextRef, currentStage);
      }

      UI* ui = GetSubsystem<UI>();
      ui->GetRoot()->RemoveAllChildren();
      this->Remove();
    }
  }
  else
    pastFirstUpdate = true;
}
