#pragma once

#include <Urho3D/Scene/Component.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Graphics/GraphicsEvents.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/UI/LineEdit.h>
#include <Urho3D/Network/Network.h>
#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/IO/PackageFile.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/UI/ProgressBar.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Core/Object.h>

#include "CustomEvents.h"

class Menu : public Urho3D::Component
{
public:

  URHO3D_OBJECT(Menu, Urho3D::Component);

  Menu(Urho3D::Context* c);

  void SetScene(Urho3D::Scene* s);

  void GenericUpdate(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData);

  //cached scene node
  Urho3D::Scene* scene_;

};
