#pragma once

#include <Urho3D/Scene/Component.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/UI/LineEdit.h>
#include <Urho3D/Network/Network.h>
#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/IO/PackageFile.h>

#include "Menu.h"
#include "PlanetLoadMenu.h"

// UDP port we will use
static const unsigned short SERVER_PORT = 2345;

class MainMenu : public Menu
{
public:

  URHO3D_OBJECT(MainMenu, Menu);

  static void RegisterObject(Urho3D::Context* c);

  MainMenu(Urho3D::Context* c);

  void HandleConnect(Urho3D::StringHash s, Urho3D::HashMap<Urho3D::StringHash, Urho3D::Variant>& h);
};
