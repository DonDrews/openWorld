#include "Menu.h"

using namespace Urho3D;

Menu::Menu(Context* c) : Component(c)
{
  SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(Menu, GenericUpdate));
}

void Menu::GenericUpdate(StringHash eventType, VariantMap& eventData)
{
  //resize to screen
  UI* ui = GetSubsystem<UI>();
  UIElement* base = ui->GetRoot()->GetChild(String("Base"));

  if(base)
  {
    Graphics* gfx = GetSubsystem<Graphics>();
    IntVector2 newSize(gfx->GetWidth(), gfx->GetHeight());
    const IntVector2 oldSize = base->GetSize();

    if(newSize != oldSize)
      base->SetSize(newSize);
  }
}

void Menu::SetScene(Scene *s)
{
  scene_ = s;
}
