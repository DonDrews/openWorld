#include "ArchiveWriter.h"

#include <iostream>

using namespace Urho3D;

void writeNodeToPackage(std::string pkgname, Node* local, Context* c)
{
  File pack(c, String(pkgname.c_str()), FILE_WRITE);

  //make vector buffer and serialize node to it
  VectorBuffer* vb = new VectorBuffer();
  local->Save(*vb);

  //calculate checksum of file
  unsigned checksum = 0;
  vb->Seek(0);
  while(!vb->IsEof())
  {
    unsigned char block[1024];
    unsigned readBytes = vb->Read(block, 1024);
    for (unsigned i = 0; i < readBytes; ++i)
      checksum = SDBMHash(checksum, block[i]);
  }

  std::cout << checksum << std::endl;

  //start laying out header of file
  pack.WriteFileID("UPAK");
  pack.WriteUInt(1); //number of files
  pack.WriteUInt(checksum); //checksum

  //write first "file" of package
  pack.WriteString("local"); //inner-file name
  pack.WriteUInt(30); //byte offset
  pack.WriteUInt(vb->GetSize()); //size
  pack.WriteUInt(checksum); //checksum

  pack.Write(vb->GetData(), vb->GetSize()); //actual data

  pack.Close();

  delete vb;
}
