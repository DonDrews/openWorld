#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Resource/XMLFile.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/DebugRenderer.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Skybox.h>
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/UI/Cursor.h>
#include <Urho3D/Engine/DebugHud.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>
#include <Urho3D/UI/Sprite.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Core/Timer.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/Resource/XMLFile.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Physics/PhysicsEvents.h>
#include <Urho3D/Physics/PhysicsWorld.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Input/Controls.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Network/Connection.h>
#include <Urho3D/Network/Network.h>
#include <Urho3D/Network/NetworkEvents.h>
#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Physics/PhysicsEvents.h>
#include <Urho3D/Physics/PhysicsWorld.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/LineEdit.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/Graphics/Terrain.h>
#include <Urho3D/IO/PackageFile.h>

#include <iostream>

#include "PlanetArea.h"
#include "Player.h"
#include "ArchiveWriter.h"
#include "ProcSky.h"
#include "Imposter.h"
#include "CustomEvents.h"

using namespace Urho3D;

// UDP port we will use
static const unsigned short SERVER_PORT = 2345;
// Identifier for our custom remote event we use to tell the client which object they control
static const StringHash E_CLIENTOBJECTID("ClientObjectID");
// Identifier for the node ID parameter in the event data
static const StringHash P_ID("ID");
/// Mapping from client connections to controllable objects.
HashMap<Connection*, WeakPtr<Node> > serverObjects_;

SharedPtr<Scene> baseArea;
SharedPtr<PlanetArea> planet;
SharedPtr<Node> local;

class OpenServer : public Application
{
public:

  OpenServer(Context* context) : Application(context)
  {}

  /// Handle connection status change (just update the buttons that should be shown.)
  void HandleConnectionStatus(StringHash eventType, VariantMap& eventData)
  {}

  virtual void Setup()
  {
	OpenConsoleWindow();
    engineParameters_["Headless"] = true;
    engineParameters_["ResourcePrefixPaths"] = "../../bin/";
  }

  Node* CreateControllableObject()
  {
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    // Create the scene node & visual representation. This will be a replicated object
    Node* charNode = baseArea->CreateChild("Player");
    Vector3 pos(0.0f, 5.0f, 0.0f);
    pos.y_ = local->GetChild("Terrain")->GetComponent<Terrain>()->GetHeight(pos) + 20.0f;
    charNode->SetPosition(pos);
    charNode->SetScale(3.0f);
    //player logic component will handle creation of all other components
    Player* play = charNode->CreateComponent<Player>();
    play->GenComponents();

    std::cout << "ID being made" << std::endl;

    return charNode;
  }

  void HandleDebugControls(Controls controls)
  {
    if(controls.buttons_ & CTRL_TIME_UP)
    {
      planet->SetTime(planet->GetTime() + 0.02f);
      URHO3D_LOGINFO("TIME UP");
    }
    if(controls.buttons_ & CTRL_TIME_DOWN)
    {
      planet->SetTime(planet->GetTime() - 0.02f);
      URHO3D_LOGINFO("TIME DOWN");
    }
  }

  void HandlePhysicsPreStep(StringHash eventType, VariantMap& eventData)
  {
    //std::cout << "Handling Physics\n";
    // This function is different on the client and server. The client collects controls (WASD controls + yaw angle)
    // and sets them to its server connection object, so that they will be sent to the server automatically at a
    // fixed rate, by default 30 FPS. The server will actually apply the controls (authoritative simulation.)
    Network* network = GetSubsystem<Network>();
    Connection* serverConnection = network->GetServerConnection();

    // Server: apply controls to client objects
    const Vector<SharedPtr<Connection> >& connections = network->GetClientConnections();

    for (unsigned i = 0; i < connections.Size(); ++i)
    {
      Connection* connection = connections[i];
      // Get the object this connection is controlling
      Node* charNode = serverObjects_[connection];
      if (!charNode)
        continue;

      RigidBody* body = charNode->GetComponent<RigidBody>();

      // Get the last controls sent by the client
      const Controls& controls = connection->GetControls();

      //first deal with any debug controls
      HandleDebugControls(controls);

      // Let logic component handle it's own physics update
      charNode->GetComponent<Player>()->applyControls(controls);
    }
  }

  void HandleDisconnect(StringHash eventType, VariantMap& eventData)
  {
    Network* network = GetSubsystem<Network>();
    Connection* serverConnection = network->GetServerConnection();

    // Or if we were running a server, stop it
    network->StopServer();
    baseArea->Clear(true, false);
  }

  void HandleClientConnected(StringHash eventType, VariantMap& eventData)
  {
    using namespace ClientConnected;

    // When a client connects, assign to scene to begin scene replication
    Connection* newConnection = static_cast<Connection*>(eventData[P_CONNECTION].GetPtr());
    newConnection->SetScene(baseArea);

    // Then create a controllable object for that client
    Node* newObject = CreateControllableObject();
    serverObjects_[newConnection] = newObject;

    // Finally send the object's node ID using a remote event
    VariantMap remoteEventData;
    remoteEventData[P_ID] = newObject->GetID();
    newConnection->SendRemoteEvent(E_CLIENTOBJECTID, true, remoteEventData);
  }

  void HandleClientDisconnected(StringHash eventType, VariantMap& eventData)
  {
    using namespace ClientConnected;

    // When a client disconnects, remove the controlled object
    Connection* connection = static_cast<Connection*>(eventData[P_CONNECTION].GetPtr());
    Node* object = serverObjects_[connection];
    if (object)
        object->Remove();

    serverObjects_.Erase(connection);
  }

  void HandleLoginInfo(StringHash eventType, VariantMap& eventData)
  {
    using namespace RemoteEventData;

    Connection* connection = static_cast<Connection*>(eventData[P_CONNECTION].GetPtr());
    Node* playerNode = serverObjects_[connection];
    playerNode->GetComponent<Player>()->SetName(eventData[LoginInfo::P_USERNAME].GetString());
  }

  virtual void Start()
  {
    //Register custom classes
    PlanetArea::RegisterObject(context_);
    Imposter::RegisterObject(context_);
    ProcSky::RegisterObject(context_);
    Player::RegisterObject(context_);

    SetRandomSeed(Time::GetSystemTime());

    //make scene
    baseArea = new Scene(context_);

    // Create octree and physics world with default settings. Create them as local so that they are not needlessly replicated
    // when a client connects
    baseArea->CreateComponent<Octree>(LOCAL);
    baseArea->CreateComponent<PhysicsWorld>(LOCAL);

    //make planet area
    planet = baseArea->CreateComponent<PlanetArea>();
    //create local nodes and save (sent over to client)
    local = planet->GenLocalContent(context_);
    writeNodeToPackage("PlanetArea.pkg", local, context_);
    planet->GenComponents();
    //set as data to be sent to client before scene replication can begin
    baseArea->AddRequiredPackageFile(new PackageFile(context_, "PlanetArea.pkg"));

    // Subscribe to fixed timestep physics updates for setting or applying controls
    SubscribeToEvent(E_PHYSICSPRESTEP, URHO3D_HANDLER(OpenServer, HandlePhysicsPreStep));

    // Subscribe to network events
    SubscribeToEvent(E_SERVERCONNECTED, URHO3D_HANDLER(OpenServer, HandleConnectionStatus));
    SubscribeToEvent(E_SERVERDISCONNECTED, URHO3D_HANDLER(OpenServer, HandleConnectionStatus));
    SubscribeToEvent(E_CONNECTFAILED, URHO3D_HANDLER(OpenServer, HandleConnectionStatus));
    SubscribeToEvent(E_CLIENTCONNECTED, URHO3D_HANDLER(OpenServer, HandleClientConnected));
    SubscribeToEvent(E_CLIENTDISCONNECTED, URHO3D_HANDLER(OpenServer, HandleClientDisconnected));
    SubscribeToEvent(E_LOGININFO, URHO3D_HANDLER(OpenServer, HandleLoginInfo));
    // Events sent between client & server (remote events) must be explicitly registered or else they are not allowed to be received
    GetSubsystem<Network>()->RegisterRemoteEvent(E_CLIENTOBJECTID);
    GetSubsystem<Network>()->RegisterRemoteEvent(E_LOGININFO);

    Network* network = GetSubsystem<Network>();
    network->StartServer(SERVER_PORT);
  }
};

URHO3D_DEFINE_APPLICATION_MAIN(OpenServer)
