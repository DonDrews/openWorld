#pragma once

#include <Urho3D/Core/Context.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/IO/File.h>

#include <string>

void writeNodeToPackage(std::string pkgname, Urho3D::Node* local, Urho3D::Context* c);
