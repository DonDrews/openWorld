#include "Uniforms.glsl"
#include "Transform.glsl"
#include "ScreenPos.glsl"
#include "Lighting.glsl"

uniform float cWindHeightFactor;
uniform float cWindHeightPivot;
uniform float cWindPeriod;
uniform vec2 cWindWorldSpacing;
uniform float cTerrSize;
uniform vec2 cTerrSpacing;

uniform float cStartDrop;
uniform float cFinishDrop;

uniform sampler2D sHeightMap1;

#ifdef NORMALMAP
    varying vec4 vTexCoord;
    varying vec4 vTangent;
#else
    varying vec2 vTexCoord;
#endif
varying vec3 vNormal;
varying vec4 vWorldPos;
#ifdef VERTEXCOLOR
    varying vec4 vColor;
#endif
#ifdef PERPIXEL
    #ifdef SHADOW
        #ifndef GL_ES
            varying vec4 vShadowPos[NUMCASCADES];
        #else
            varying highp vec4 vShadowPos[NUMCASCADES];
        #endif
    #endif
    #ifdef SPOTLIGHT
        varying vec4 vSpotPos;
    #endif
    #ifdef POINTLIGHT
        varying vec3 vCubeMaskVec;
    #endif
#else
    varying vec3 vVertexLight;
    varying vec4 vScreenPos;
    #ifdef ENVCUBEMAP
        varying vec3 vReflectionVec;
    #endif
    #if defined(LIGHTMAP) || defined(AO)
        varying vec2 vTexCoord2;
    #endif
#endif

void VS()
{
    mat4 modelMatrix = iModelMatrix;
    vec3 worldPos = GetWorldPos(modelMatrix);
    vec2 sampleLoc = ((vec2(worldPos.x, -worldPos.z) + (cTerrSpacing.x / 2.0)) / cTerrSize) + vec2(-0.25, -0.25);
    ivec2 intSample = ivec2(sampleLoc);
    vec2 diffSample = sampleLoc - intSample;

    vec4 heightmapTL = texelFetch(sHeightMap1, ivec2(sampleLoc.x, sampleLoc.y), 0);
    vec4 heightmapTR = texelFetch(sHeightMap1, ivec2(sampleLoc.x + 1, sampleLoc.y), 0);
    vec4 heightmapBL = texelFetch(sHeightMap1, ivec2(sampleLoc.x, sampleLoc.y + 1), 0);
    vec4 heightmapBR = texelFetch(sHeightMap1, ivec2(sampleLoc.x + 1, sampleLoc.y + 1), 0);

    float heightTL = cTerrSpacing.y * (floor(heightmapTL.r * 256) + heightmapTL.g);
    float heightTR = cTerrSpacing.y * (floor(heightmapTR.r * 256) + heightmapTR.g);
    float heightBL = cTerrSpacing.y * (floor(heightmapBL.r * 256) + heightmapBL.g);
    float heightBR = cTerrSpacing.y * (floor(heightmapBR.r * 256) + heightmapBR.g);

    float heightT = mix(heightTL, heightTR, diffSample.x);
    float heightB = mix(heightBL, heightBR, diffSample.x);

    float height = mix(heightT, heightB, diffSample.y);

    float vertHeight = (worldPos.y - modelMatrix[1][3]) / 10.0f;

    float windStrength = max(vertHeight - cWindHeightPivot, 0.0) * cWindHeightFactor;
    float windPeriod = cElapsedTime * cWindPeriod * 2 + dot(worldPos.xz, cWindWorldSpacing);
    float windPeriodSmall = cElapsedTime * cWindPeriod * 4 + dot(worldPos.xz, cWindWorldSpacing * 0.25);
    //worldPos.x += windStrength * (cos(windPeriodSmall + 0.5) * 0.5 + cos(windPeriod));
    //worldPos.z -= windStrength * (cos(windPeriodSmall) * 0.5 + sin(windPeriod));
    float pi = 3.1415926;
    worldPos.x += windStrength * (0.5 * (sin(windPeriod)) + 0.3 * (sin(windPeriod * 3.5)));
    worldPos.z -= windStrength * (0.5 * (cos(windPeriod)) + 0.3 * (cos(windPeriod * 2)));

    worldPos.y = height + vertHeight;
    float dist = GetDepth(GetClipPos(worldPos)) * (cFarClip - cNearClip) - cNearClip;
    worldPos.y += clamp(sqrt((cFinishDrop - dist) / (cFinishDrop - cStartDrop)), 0.0, 1.0);
    worldPos.y -= 1.0;

    gl_Position = GetClipPos(worldPos);
    vNormal = GetWorldNormal(modelMatrix);
    vWorldPos = vec4(worldPos, GetDepth(gl_Position));

    #ifdef VERTEXCOLOR
        vColor = iColor;
    #endif

    #ifdef NORMALMAP
        vec3 tangent = GetWorldTangent(modelMatrix);
        vec3 bitangent = cross(tangent, vNormal) * iTangent.w;
        vTexCoord = vec4(GetTexCoord(iTexCoord), bitangent.xy);
        vTangent = vec4(tangent, bitangent.z);
    #else
        vTexCoord = GetTexCoord(iTexCoord);
    #endif

    #ifdef PERPIXEL
        // Per-pixel forward lighting
        vec4 projWorldPos = vec4(worldPos, 1.0);

        #ifdef SHADOW
            // Shadow projection: transform from world space to shadow space
            for (int i = 0; i < NUMCASCADES; i++)
                vShadowPos[i] = GetShadowPos(i, vNormal, projWorldPos);
        #endif

        #ifdef SPOTLIGHT
            // Spotlight projection: transform from world space to projector texture coordinates
            vSpotPos = projWorldPos * cLightMatrices[0];
        #endif

        #ifdef POINTLIGHT
            vCubeMaskVec = (worldPos - cLightPos.xyz) * mat3(cLightMatrices[0][0].xyz, cLightMatrices[0][1].xyz, cLightMatrices[0][2].xyz);
        #endif
    #else
        // Ambient & per-vertex lighting
        #if defined(LIGHTMAP) || defined(AO)
            // If using lightmap, disregard zone ambient light
            // If using AO, calculate ambient in the PS
            vVertexLight = vec3(0.0, 0.0, 0.0);
            vTexCoord2 = iTexCoord1;
        #else
            vVertexLight = GetAmbient(GetZonePos(worldPos));
        #endif

        #ifdef NUMVERTEXLIGHTS
            for (int i = 0; i < NUMVERTEXLIGHTS; ++i)
                vVertexLight += GetVertexLight(i, worldPos, vNormal) * cVertexLights[i * 3].rgb;
        #endif

        vScreenPos = GetScreenPos(gl_Position);

        #ifdef ENVCUBEMAP
            vReflectionVec = worldPos - cCameraPos;
        #endif
    #endif
}
