#include "Uniforms.glsl"
#include "Transform.glsl"

uniform float cWindHeightFactor;
uniform float cWindHeightPivot;
uniform float cWindPeriod;
uniform vec2 cWindWorldSpacing;
uniform float cTerrSize;
uniform vec2 cTerrSpacing;

uniform float cStartDrop;
uniform float cFinishDrop;

uniform sampler2D sHeightMap1;

varying vec3 vTexCoord;

void VS()
{
  mat4 modelMatrix = iModelMatrix;
  vec3 worldPos = GetWorldPos(modelMatrix);
  vec2 sampleLoc = ((vec2(worldPos.x, -worldPos.z) + (cTerrSpacing.x / 2.0)) / cTerrSize) + vec2(-0.25, -0.25);
  ivec2 intSample = ivec2(sampleLoc);
  vec2 diffSample = sampleLoc - intSample;

  vec4 heightmapTL = texelFetch(sHeightMap1, ivec2(sampleLoc.x, sampleLoc.y), 0);
  vec4 heightmapTR = texelFetch(sHeightMap1, ivec2(sampleLoc.x + 1, sampleLoc.y), 0);
  vec4 heightmapBL = texelFetch(sHeightMap1, ivec2(sampleLoc.x, sampleLoc.y + 1), 0);
  vec4 heightmapBR = texelFetch(sHeightMap1, ivec2(sampleLoc.x + 1, sampleLoc.y + 1), 0);

  float heightTL = cTerrSpacing.y * (floor(heightmapTL.r * 256) + heightmapTL.g);
  float heightTR = cTerrSpacing.y * (floor(heightmapTR.r * 256) + heightmapTR.g);
  float heightBL = cTerrSpacing.y * (floor(heightmapBL.r * 256) + heightmapBL.g);
  float heightBR = cTerrSpacing.y * (floor(heightmapBR.r * 256) + heightmapBR.g);

  float heightT = mix(heightTL, heightTR, diffSample.x);
  float heightB = mix(heightBL, heightBR, diffSample.x);

  float height = mix(heightT, heightB, diffSample.y);

  float vertHeight = (worldPos.y - modelMatrix[1][3]) / 10.0f;

  float windStrength = max(vertHeight - cWindHeightPivot, 0.0) * cWindHeightFactor;
  float windPeriod = cElapsedTime * cWindPeriod * 2 + dot(worldPos.xz, cWindWorldSpacing);
  float windPeriodSmall = cElapsedTime * cWindPeriod * 4 + dot(worldPos.xz, cWindWorldSpacing * 0.25);
  worldPos.x += windStrength * (cos(windPeriodSmall + 0.5) * 0.5 + cos(windPeriod));
  worldPos.z -= windStrength * (cos(windPeriodSmall) * 0.5 + sin(windPeriod));

  worldPos.y = height + vertHeight;
  float dist = GetDepth(GetClipPos(worldPos)) * (cFarClip - cNearClip) - cNearClip;
  worldPos.y += clamp(sqrt((cFinishDrop - dist) / (cFinishDrop - cStartDrop)), 0.0, 1.0);
  worldPos.y -= 1.0;

  gl_Position = GetClipPos(worldPos);
  vTexCoord = vec3(GetTexCoord(iTexCoord), GetDepth(gl_Position));
}
