#ifdef COMPILEPS
float shouldFade(float dist, float fadeStart, float fadeEnd, vec2 samplePos)
{
  //determines if a given pixel should be faded out
  //1.0 if rendered, 0.0 if discarded

  //convert distance from clip-space to world-space
  dist = mix(cNearClipPS, cFarClipPS, dist);
  float threshold = (dist - fadeStart) / (fadeEnd - fadeStart);

  return float(texture(sEmissiveMap, samplePos).r > clamp(threshold, 0.0, 1.0));
}
#endif
