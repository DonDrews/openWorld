// DEEP_WATER: Based on Water.glsl

#include "Uniforms.glsl"
#include "Samplers.glsl"
#include "Transform.glsl"
#include "ScreenPos.glsl"
#include "Lighting.glsl"
#include "Fog.glsl"

#ifndef GL_ES
varying vec4 vScreenPos;
varying vec2 vReflectUV;
varying vec2 vWaterUV;
varying vec3 vEyeVec;
varying vec3 vEyePos;
varying vec4 vWorldPos;

varying mat4 viewProj;
varying vec4 bufferOffsets;
#else
varying highp vec4 vScreenPos;
varying highp vec2 vReflectUV;
varying highp vec2 vWaterUV;
varying highp vec3 vEyeVec;
varying highp vec4 vWorldPos;
#endif
varying vec3 vNormal;

#ifdef COMPILEVS
uniform vec2 cNoiseSpeed;
uniform float cNoiseTiling;
#endif

#ifdef COMPILEPS
uniform float cNoiseStrength;
uniform float cFresnelPower;
uniform vec3 cShallowColor;
uniform vec3 cDeepColor;
uniform float cDepthScale;
#endif


#ifdef SPOTLIGHT
    varying vec4 vSpotPos;
#endif
#ifdef POINTLIGHT
    varying vec3 vCubeMaskVec;
#endif

varying vec4 vTangent;
varying vec4 vTexCoord;
varying vec4 vTexCoord2;


void VS()
{
    mat4 modelMatrix = iModelMatrix;
    vec3 worldPos = GetWorldPos(modelMatrix);
    gl_Position = GetClipPos(worldPos);
    vWorldPos = vec4(worldPos, GetDepth(gl_Position));
    vNormal = GetWorldNormal(modelMatrix);
    vScreenPos = GetScreenPos(gl_Position);

    vReflectUV = GetQuadTexCoord(gl_Position);
    vReflectUV.y = 1.0 - vReflectUV.y;
    vReflectUV *= gl_Position.w;
    vWaterUV = iTexCoord * cNoiseTiling + cElapsedTime * cNoiseSpeed;
    vEyeVec = cCameraPos - worldPos;

    bufferOffsets = cGBufferOffsets;
    viewProj = cViewProj;
    vEyePos = cCameraPos;

    // Per-pixel forward lighting
    vec4 projWorldPos = vec4(worldPos, 1.0);

    vec4 tangent = GetWorldTangent(modelMatrix);
    vec3 bitangent = cross(tangent.xyz, vNormal) * tangent.w;
    vTexCoord = vec4(GetTexCoord(iTexCoord * cNoiseTiling + cElapsedTime * cNoiseSpeed), bitangent.xy);
    vTexCoord2 = vec4(GetTexCoord(iTexCoord.yx * cNoiseTiling - cElapsedTime * cNoiseSpeed), bitangent.xy);
    vTangent = vec4(tangent.xyz, bitangent.z);

    #ifdef SPOTLIGHT
        // Spotlight projection: transform from world space to projector texture coordinates
        vSpotPos = projWorldPos * cLightMatrices[0];
    #endif

    #ifdef POINTLIGHT
        vCubeMaskVec = (worldPos - cLightPos.xyz) * mat3(cLightMatrices[0][0].xyz, cLightMatrices[0][1].xyz, cLightMatrices[0][2].xyz);
    #endif
}

#ifdef COMPILEPS

vec4 GetScreenPos(vec4 clipPos)
{
    return vec4(
        clipPos.x * bufferOffsets.z + bufferOffsets.x * clipPos.w,
        clipPos.y * bufferOffsets.w + bufferOffsets.y * clipPos.w,
        0.0,
        clipPos.w);
}

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

#line 110

//Donovan's Test SSR
bool castRay(vec3 startWorld, vec3 dirWorld, sampler2D depthMap, mat4 viewProjection, out vec2 screenPosHit)
{
  const float MAX_DIST = 5000;
  const float NUM_ITER = 49;
  float THICKNESS = 30;
  const int NUM_SEARCH = 3;
  vec3 endWorld = startWorld + dirWorld * MAX_DIST;

  vec4 startClip = vec4(startWorld, 1) * viewProjection;
  vec4 endClip = vec4(endWorld, 1) * viewProjection;
  vec4 dClip = (endClip - startClip) / NUM_ITER;

  //lots of variables for determining final ray scaling
  vec2 slope = dClip.xy / dClip.w;
  vec2 intercept = startClip.xy - (slope * startClip.w);

  //determine scaling necessary to make fit for each coordinate independently
  float z_int[] = float[4](0.0, 0.0, 0.0, 0.0);

  z_int[0] = (slope.x > 1.0) ? intercept.x / (1.0 - slope.x) : endClip.w;
  z_int[1] = (slope.x < -1.0) ? intercept.x / (-1.0 - slope.x) : endClip.w;
  z_int[2] = (slope.y > 1.0) ? intercept.y / (1.0 - slope.y) : endClip.w;
  z_int[3] = (slope.y < -1.0) ? intercept.y / (-1.0 - slope.y) : endClip.w;

  float finalz_int = endClip.w;
  for(int i = 0; i < 4; i++)
  {
    finalz_int = min(finalz_int, (z_int[i] > 0) ? z_int[i] : endClip.w);
  }

  float scale = finalz_int / endClip.w;

  //gl_FragColor = vec4(z_int[0] / 10, z_int[1] / 10, 0.0, 0.0);

  endClip *= scale;

  gl_FragColor = vec4(scale, 0.0, 0.0, 0.0);

  //redo projection based on new scale of ray
  //endWorld = startWorld + (dirWorld * MAX_DIST * finalScale);
  //endClip = vec4(endWorld, 1) * viewProjection;

  dClip = (endClip - startClip) / NUM_ITER;

  float jitter = rand(startWorld.xy) * 0.1;

  float lastLine;
  float lastSample;
  vec2 lastScreen;



  for(int i = 0; i < NUM_ITER; i++)
  {
    THICKNESS += 30;
    vec4 samplePosClip = startClip + dClip * (i);
    vec2 sampleScreen = GetScreenPos(samplePosClip).xy / GetScreenPos(samplePosClip).w;
    float sampleDepth = ReconstructDepth(texture2D(depthMap, sampleScreen).r);
    float lineDepth = samplePosClip.w / cFarClipPS;

    bool withinScreen = max(abs(sampleScreen - 0.5).x, abs(sampleScreen - 0.5).y) < 0.5;

    if(sampleDepth < lineDepth && lineDepth - (THICKNESS / cFarClipPS) < sampleDepth)
    {
      //we hit something
      if(i != 0)
      {
        /*
        use the previous intersection point to do a binary search for the true intersection
        */
        float mixVal = 0.5;
        for(int j = 0; j < NUM_SEARCH; j++)
        {
          vec2 midScreen = mix(lastScreen, sampleScreen, mixVal);
          float midLineDepth = mix(lastLine, lineDepth, mixVal);

          float midSampleDepth = ReconstructDepth(texture2D(depthMap, midScreen).r);

          //look for intersection at current midpoint
          if(midSampleDepth < midLineDepth && midLineDepth - (THICKNESS / cFarClipPS) < midSampleDepth)
          {
            mixVal -= 0.25 / pow(2, j);
          }
          else
          {
            mixVal += 0.25 / pow(2, j);
          }
        }

        screenPosHit = mix(lastScreen, sampleScreen, mixVal);
      }
      else
      {
        //no earlier iteration to interpolate based on
        screenPosHit = sampleScreen;
      }
      return true;
    }

    lastLine = lineDepth;
    lastSample = sampleDepth;
    lastScreen = sampleScreen;
  }

  return false;
}

vec3 FixCubeLookup(vec3 v)
{
    float M = max(max(abs(v.x), abs(v.y)), abs(v.z));
    float scale = (1024 - 1) / 1024;

    if (abs(v.x) != M) v.x += scale;
    if (abs(v.y) != M) v.y += scale;
    if (abs(v.z) != M) v.z += scale;

    return v;
}

void PS()
{
    #ifdef PERPIXEL

        #if defined(SPOTLIGHT)
            vec3 lightColor = vSpotPos.w > 0.0 ? texture2DProj(sLightSpotMap, vSpotPos).rgb * cLightColor.rgb : vec3(0.0, 0.0, 0.0);
        #elif defined(CUBEMASK)
            vec3 lightColor = textureCube(sLightCubeMap, vCubeMaskVec).rgb * cLightColor.rgb;
        #else
            vec3 lightColor = cLightColor.rgb;
        #endif

        #ifdef DIRLIGHT
            vec3 lightDir = cLightDirPS;
        #else
            vec3 lightVec = (cLightPosPS.xyz - vWorldPos.xyz) * cLightPosPS.w;
            vec3 lightDir = normalize(lightVec);
        #endif

        mat3 tbn = mat3(vTangent.xyz, vec3(vTexCoord.zw, vTangent.w), vNormal);
        vec3 normal = normalize(tbn * DecodeNormal(texture2D(sNormalMap, vTexCoord.xy)));
        vec3 normal2 = normalize(tbn * DecodeNormal(texture2D(sNormalMap, vTexCoord2.xy)));
        normal = normalize(normal + normal2);

        #ifdef HEIGHTFOG
            float fogFactor = GetHeightFogFactor(vWorldPos.w, vWorldPos.y);
        #else
            float fogFactor = GetFogFactor(vWorldPos.w);
        #endif

        vec3 spec = GetSpecular(normal, cCameraPosPS - vWorldPos.xyz, lightDir, 200.0) * lightColor * cLightColor.a;

        //gl_FragColor = vec4(GetLitFog(spec, fogFactor), 1.0);

    #else

        vec2 refractUV = vScreenPos.xy / vScreenPos.w;

        mat3 tbn = mat3(vTangent.xyz, vec3(vTexCoord.zw, vTangent.w), vNormal);
        vec3 normal = normalize(tbn * DecodeNormal(texture2D(sNormalMap, vTexCoord.xy)));

        vec2 noise = (texture2D(sNormalMap, vTexCoord.xy).rg - 0.5) * cNoiseStrength;
        refractUV += noise;

        float fresnel = pow(1.0 - clamp(dot(normalize(vEyeVec), vNormal), 0.0, 1.0), cFresnelPower);
        vec3 refractColor = texture2D(sEnvMap, refractUV).rgb;

        vec4 depthInput = texture2D(sDepthBuffer, refractUV);
        float depth = ReconstructDepth(depthInput.r); // HWDEPTH
        float waterDepth = (depth - vWorldPos.w) * (cFarClipPS - cNearClipPS);

        // Object above water. Recalc without UV noise (avoid artefacts).
        if (waterDepth <= 0.0)
        {
            refractColor = texture2D(sEnvMap, vScreenPos.xy / vScreenPos.w).rgb;

            depthInput = texture2D(sDepthBuffer, vScreenPos.xy / vScreenPos.w);
            depth = ReconstructDepth(depthInput.r);
            waterDepth = (depth - vWorldPos.w) * (cFarClipPS - cNearClipPS);
        }

        vec3 reflectColor = vec3(0.0, 0.0, 0.0);

        vec3 camReflectRay = normalize(reflect(vWorldPos.xyz - vEyePos, normalize(normal)));
        vec2 hitPixel;
        bool succ = castRay(vWorldPos.xyz, camReflectRay, sDepthBuffer, viewProj, hitPixel);

        if(succ)
          reflectColor = texture2D(sEnvMap, hitPixel).rgb;
        else
          reflectColor = textureLod(sZoneCubeMap, FixCubeLookup(camReflectRay), 0).rgb * (cAmbientColor.a + 1.0);

        vec3 waterColor = mix(cShallowColor, cDeepColor, clamp(waterDepth * cDepthScale, 0.0, 1.0));
        refractColor *= waterColor;
        vec3 finalColor = mix(refractColor, reflectColor, fresnel);
        gl_FragColor = vec4(GetFog(finalColor, GetFogFactor(vWorldPos.w)), 1.0);

    #endif
}

#endif
