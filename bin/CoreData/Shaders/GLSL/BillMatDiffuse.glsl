#include "Uniforms.glsl"
#include "Samplers.glsl"
#include "Transform.glsl"
#include "ScreenPos.glsl"
#include "Lighting.glsl"
#include "Fog.glsl"
#include "ThresholdFade.glsl"

const float TAU = 6.283185307;

varying vec2 vTexCoord;
varying vec4 vWorldPos;
#ifdef VERTEXCOLOR
    varying vec4 vColor;
#endif
#ifdef PERPIXEL
    #ifdef SPOTLIGHT
        varying vec4 vSpotPos;
    #endif
    #ifdef POINTLIGHT
        varying vec3 vCubeMaskVec;
    #endif
#else
    varying vec3 vVertexLight;
    varying vec4 vScreenPos;
    #if defined(LIGHTMAP) || defined(AO)
        varying vec2 vTexCoord2;
    #endif
#endif

#ifdef FADE
  uniform float cStartFade;
  uniform float cEndFade;
#endif

void PS()
{
  //remove if alpha is too low, or if fade texture dictates that it should be.
  float removalCoeff = 1.0;

  //find the texture coord we are sampling from
  vec3 posDiff = cCameraPosPS.xyz - vWorldPos.xyz;
  float samplingValue = fract(atan(-posDiff.x, posDiff.z) / TAU + 1.25 + vColor.z) * 8;
  vec2 sample_1 = vec2((floor(samplingValue) + vTexCoord.x) / 8, vTexCoord.y);
  vec2 sample_2 = vec2((floor(samplingValue + 1) + vTexCoord.x) / 8, vTexCoord.y);
  float blend = samplingValue - floor(samplingValue);

  // Get material diffuse albedo
  vec4 diffInput = mix(texture2D(sDiffMap, sample_1), texture2D(sDiffMap, sample_2), blend);
  removalCoeff = diffInput.a;

  vec4 diffColor = diffInput;

  //calculate rotation matrix for normals
  mat3 normalRot;
  normalRot[0] = vec3(vColor.x, 0, -vColor.y); //col 1
  normalRot[1] = vec3(0, 1, 0); //col 2
  normalRot[2] = vec3(vColor.y, 0, vColor.x); //col 3

  // Get normal
  vec3 normal = normalize(normalRot * DecodeNormal(mix(texture2D(sNormalMap, sample_1), texture2D(sNormalMap, sample_2), blend)));

  // Get fog factor
  #ifdef HEIGHTFOG
      float fogFactor = GetHeightFogFactor(vWorldPos.w, vWorldPos.y);
  #else
      float fogFactor = GetFogFactor(vWorldPos.w);
  #endif

  #ifdef FADE
    removalCoeff *= shouldFade(vWorldPos.w, cStartFade, cEndFade, vec2(vTexCoord));
  #endif

  if(removalCoeff < 0.5)
    discard;

  #if defined(PERPIXEL)
      // Per-pixel forward lighting
      vec3 lightColor;
      vec3 lightDir;
      vec3 finalColor;

      float diff = GetDiffuse(normal, vWorldPos.xyz, lightDir);

      #if defined(SPOTLIGHT)
          lightColor = vSpotPos.w > 0.0 ? texture2DProj(sLightSpotMap, vSpotPos).rgb * cLightColor.rgb : vec3(0.0, 0.0, 0.0);
      #elif defined(CUBEMASK)
          lightColor = textureCube(sLightCubeMap, vCubeMaskVec).rgb * cLightColor.rgb;
      #else
          lightColor = cLightColor.rgb;
      #endif

      finalColor = diff * lightColor * diffColor.rgb;

      #ifdef AMBIENT
          finalColor += cAmbientColor.rgb * diffColor.rgb;
          finalColor += cMatEmissiveColor;
          gl_FragColor = vec4(GetFog(finalColor, fogFactor), diffColor.a);
      #else
          gl_FragColor = vec4(GetLitFog(finalColor, fogFactor), diffColor.a);
      #endif
  #elif defined(PREPASS)
      // Fill light pre-pass G-Buffer
      float specPower = cMatSpecColor.a / 255.0;

      gl_FragData[0] = vec4(normal * 0.5 + 0.5, specPower);
      gl_FragData[1] = vec4(EncodeDepth(vWorldPos.w), 0.0);
  #elif defined(DEFERRED)
      // Fill deferred G-buffer
      float specIntensity = 0;
      float specPower = cMatSpecColor.a / 255.0;

      vec3 finalColor = vVertexLight * diffColor.rgb;
      #ifdef AO
          // If using AO, the vertex light ambient is black, calculate occluded ambient here
          finalColor += texture2D(sEmissiveMap, vTexCoord2).rgb * cAmbientColor.rgb * diffColor.rgb;
      #endif

      #ifdef ENVCUBEMAP
          finalColor += cMatEnvMapColor * textureCube(sEnvCubeMap, reflect(vReflectionVec, normal)).rgb;
      #endif
      #ifdef LIGHTMAP
          finalColor += texture2D(sEmissiveMap, vTexCoord2).rgb * diffColor.rgb;
      #endif
      #ifdef EMISSIVEMAP
          finalColor += cMatEmissiveColor * texture2D(sEmissiveMap, vTexCoord.xy).rgb;
      #else
          finalColor += cMatEmissiveColor;
      #endif

      gl_FragData[0] = vec4(GetFog(finalColor, fogFactor), 1.0);
      gl_FragData[1] = fogFactor * vec4(diffColor.rgb, specIntensity);
      gl_FragData[2] = vec4(normal * 0.5 + 0.5, specPower);
      gl_FragData[3] = vec4(EncodeDepth(vWorldPos.w), 0.0);
  #else
      // Ambient & per-vertex lighting
      vec3 finalColor = vVertexLight * diffColor.rgb;
      #ifdef AO
          // If using AO, the vertex light ambient is black, calculate occluded ambient here
          finalColor += texture2D(sEmissiveMap, vTexCoord2).rgb * cAmbientColor.rgb * diffColor.rgb;
      #endif

      #ifdef MATERIAL
          // Add light pre-pass accumulation result
          // Lights are accumulated at half intensity. Bring back to full intensity now
          vec4 lightInput = 2.0 * texture2DProj(sLightBuffer, vScreenPos);
          vec3 lightSpecColor = lightInput.a * lightInput.rgb / max(GetIntensity(lightInput.rgb), 0.001);

          finalColor += lightInput.rgb * diffColor.rgb + 0;
      #endif

      #ifdef ENVCUBEMAP
          finalColor += cMatEnvMapColor * textureCube(sEnvCubeMap, reflect(vReflectionVec, normal)).rgb;
      #endif
      #ifdef LIGHTMAP
          finalColor += texture2D(sEmissiveMap, vTexCoord2).rgb * diffColor.rgb;
      #endif
      #ifdef EMISSIVEMAP
          finalColor += cMatEmissiveColor * texture2D(sEmissiveMap, vTexCoord.xy).rgb;
      #else
          finalColor += cMatEmissiveColor;
      #endif

      gl_FragColor = vec4(GetFog(finalColor, fogFactor), diffColor.a);
  #endif
}
