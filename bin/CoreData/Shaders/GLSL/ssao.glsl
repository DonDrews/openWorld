#include "Uniforms.glsl"
#include "Samplers.glsl"
#include "Transform.glsl"
#include "ScreenPos.glsl"

varying highp vec2 vScreenPos;
varying mat4 vViewProj;
varying vec4 vGBufferOffsets;

#ifdef COMPILEVS

void VS()
{
  mat4 modelMatrix = iModelMatrix;
  vec3 worldPos = GetWorldPos(modelMatrix);
  gl_Position = GetClipPos(worldPos);
  vScreenPos = GetScreenPosPreDiv(gl_Position);
  vViewProj = cViewProj;
  vGBufferOffsets = cGBufferOffsets;
}

#endif


#ifdef COMPILEPS
uniform highp vec2 cScreenSize;

// Port from: https://github.com/jsj2008/Zombie-Blobs/blob/278e16229ccb77b2e11d788082b2ccebb9722ace/src/postproc.fs

// see T Möller, 1999: Efficiently building a matrix to rotate one vector to another
mat3 rotateNormalVecToAnother(vec3 f, vec3 t) {
  vec3 v = cross(f, t);
  float c = dot(f, t);
  float h = (1.0 - c) / (1.0 - c * c);
  return mat3(c + h * v.x * v.x, h * v.x * v.y + v.z, h * v.x * v.z - v.y,
              h * v.x * v.y - v.z, c + h * v.y * v.y, h * v.y * v.z + v.x,
              h * v.x * v.z + v.y, h * v.y * v.z - v.x, c + h * v.z * v.z);
}

vec2 ToScreenSpace(vec4 clipPos, vec4 bOffsets)
{
    return vec2(
        clipPos.x / clipPos.w * bOffsets.z + bOffsets.x,
        clipPos.y / clipPos.w * bOffsets.w + bOffsets.y);
}

vec3 normal_from_depth(float depth, highp vec2 texcoords) {
  // One pixel: 0.001 = 1 / 1000 (pixels)
  const vec2 offset1 = vec2(0.0, 0.001);
  const vec2 offset2 = vec2(0.001, 0.0);

  float depth1 = DecodeDepth(texture2D(sEmissiveMap, texcoords + offset1).rgb);
  float depth2 = DecodeDepth(texture2D(sEmissiveMap, texcoords + offset2).rgb);

  vec3 p1 = vec3(offset1, depth1 - depth);
  vec3 p2 = vec3(offset2, depth2 - depth);

  highp vec3 normal = cross(p1, p2);
  normal.z = -normal.z;

  return normalize(normal);
}

void PS()
{
  vec3 S_PTS[8];

  S_PTS[0] = vec3(0.23066101796436578, -0.011036813863513495, -0.26950400797618296);
  S_PTS[1] = vec3(0.25621837699170646, 0.6710475241772148, -0.033285754100040796);
  S_PTS[2] = vec3(0.3619665900831091, 0.05262789104448323, 0.11360191557377709);
  S_PTS[3] = vec3(0.10842191170857561, 0.4631666165215175, 0.0966473521894363);
  S_PTS[4] = vec3(0.17081052453864679, -0.7348919669014748, -0.19904974506073483);
  S_PTS[5] = vec3(0.5166833325294273, -0.38311137701980413, -0.2511568751136024);
  S_PTS[6] = vec3(0.29018739923026904, -0.5458730819652207, -0.6661544899982965);
  S_PTS[7] = vec3(0.9798620259214963, 0.03778206704652008, -0.049914252587563705);

  //constant parameters
  const float aoStrength = 3.0;
  const float radius = 1.0;

  float depth = DecodeDepth(texture2D(sEmissiveMap, vScreenPos).rgb);
  vec2 revisedXY = vec2(vScreenPos.x * 2 - 1, (1 - vScreenPos.y) * 2 - 1);
  mat4 invProj = inverse(vViewProj);
  vec4 viewPos = vec4(revisedXY, depth, 1.0) * invProj;
  //transform from homogenous to standard coords
  viewPos = vec4(viewPos.xyz / viewPos.w, 1.0);
  //viewPos = vec4(viewPos.xy, depth, 1.0);

  float totalOcc = 0;

  for(int i = 0; i < 8; i++)
  {
    //for each point in the sample radius
    vec4 offsetPoint = vec4(viewPos.xyz * (S_PTS[i] * radius), 1.0);
    vec2 screenOffset = ToScreenSpace(offsetPoint * vViewProj, vGBufferOffsets);
    float measuredDepth = DecodeDepth(texture2D(sEmissiveMap, screenOffset).rgb);
    float sampleDepth = (offsetPoint.z - cNearClipPS) / (cFarClipPS - cNearClipPS);

    //1.0 if sampled depth is larger than measured depth (is occluded), 0.0 if otherwise
    totalOcc += float(max(measuredDepth - sampleDepth, 0.0) == 0.0);
  }

  float ao = totalOcc / 8.0;
  float d = DecodeDepth(texture2D(sEmissiveMap, vScreenPos).rgb) * 200;

  if(vScreenPos.x < 0.5)
  {
    gl_FragColor = vec4(texture2D(sDiffMap, vScreenPos).rgb, 1.0);
  }
  else
  {
    gl_FragColor = vec4(viewPos.xyz, 1.0);
  }
}

#endif
