#include "Imposter.h"

using namespace Urho3D;

Imposter::Imposter(Context* c) : BillboardSet(c)
{}

Imposter::~Imposter()
{

}

void Imposter::RegisterObject(Context* context)
{
  context->RegisterFactory<Imposter>();

  URHO3D_COPY_BASE_ATTRIBUTES(BillboardSet);
  URHO3D_MIXED_ACCESSOR_ATTRIBUTE("ImposterModel", GetImpModAttr, SetImpModAttr, ResourceRef, ResourceRef(Model::GetTypeStatic()), AM_DEFAULT);
  URHO3D_MIXED_ACCESSOR_ATTRIBUTE("ImposterMaterial", GetImpMatAttr, SetImpMatAttr, ResourceRef, ResourceRef(Material::GetTypeStatic()), AM_DEFAULT);
}

//SLOW: only run this once (or when absolutely necessary)
void Imposter::RenderTextures(Context* context, Stage stage)
{
  const IntVector2 TEX_SIZE(400, 150);

  if(stage == IMPOSTER_SETUP)
  {
    std::cout << "Setup" << std::endl;
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    //make a scene
    renderScene = new Scene(context);
    renderScene->CreateComponent<Octree>();

    //add model to center
    Node* modelNode = renderScene->CreateChild("Mod");
    renderMod = modelNode->CreateComponent<StaticModel>();
    std::cerr << "Names: " << model.name_.CString() << " " << modelMat.name_.CString() << std::endl;
    renderMod->SetModel(cache->GetResource<Model>(model.name_));
    newMat = cache->GetResource<Material>(modelMat.name_);
    Vector2 plane = GetRenderPlaneSize(renderMod->GetBoundingBox());

    //change material to use new pseudo-deferred shaders
    passes = newMat->GetTechnique(0)->GetPasses();
    oldShaders = PODVector<String*>(passes.Size());
    for(int i = 0; i < passes.Size(); i++)
    {
      oldShaders[i] = new String(passes[i]->GetPixelShader());
      passes[i]->SetPixelShader("ImposterDefault");
    }
    renderMod->SetMaterial(newMat);

    //make set of cameras for one texture
    Node* cameraNodes[CAM_AMOUNT];
    Camera* cameras[CAM_AMOUNT];
    for(unsigned i = 0; i < CAM_AMOUNT; i++)
    {
      cameraNodes[i] = renderScene->CreateChild("Cam");
      cameraNodes[i]->SetPosition(Vector3(cos((float(i) / float(CAM_AMOUNT)) * 2.0 * M_PI), plane.y_ / 2, sin((float(i) / float(CAM_AMOUNT)) * 2.0 * M_PI)));
      cameraNodes[i]->SetRotation(Quaternion(0, 270 - (360 / CAM_AMOUNT) * i, 0));
      std::cout << "Cam Number " << i << std::endl;
      std::cout << cameraNodes[i]->GetPosition().x_ << std::endl;
      std::cout << cameraNodes[i]->GetPosition().y_ << std::endl;
      std::cout << cameraNodes[i]->GetPosition().z_ << std::endl;
      std::cout << cameraNodes[i]->GetRotation().EulerAngles().y_ << std::endl;
      cameras[i] = cameraNodes[i]->CreateComponent<Camera>();
      cameras[i]->SetOrthographic(true);
      cameras[i]->SetOrthoSize(plane);
    }

    //make renderable texture and bind cameras
    renderTexture = SharedPtr<Texture2D>(new Texture2D(context));
    renderTexture->SetSize(TEX_SIZE.x_, TEX_SIZE.y_, Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
    renderTexture->SetFilterMode(FILTER_BILINEAR);
    rs = renderTexture->GetRenderSurface();
    rs->SetUpdateMode(SURFACE_MANUALUPDATE);
    rs->SetNumViewports(CAM_AMOUNT);
    Viewport* ports[CAM_AMOUNT];

    RenderPath* path = new RenderPath();
    path->Load(cache->GetResource<XMLFile>("RenderPaths/ForwardClear.xml"));

    for(int i = 0; i < CAM_AMOUNT; i++)
    {
      int minX = (i * TEX_SIZE.x_) / CAM_AMOUNT;
      int maxX = ((i + 1) * TEX_SIZE.x_) / CAM_AMOUNT;
      ports[i] = new Viewport(context, renderScene, cameras[i], IntRect(minX, 0, maxX, TEX_SIZE.y_), path);

      rs->SetViewport(i, ports[i]);
    }

    //setup CPU side image
    renderedImage = SharedPtr<Image>(new Image(context));
    renderedImage->SetSize(TEX_SIZE.x_, TEX_SIZE.y_, renderTexture->GetComponents());

    //FIRST RENDER
    rs->QueueUpdate();
  }
  else if(stage == IMPOSTER_FIRST)
  {
    std::cout << "First" << std::endl;
    //save image
    renderTexture->GetData(0, renderedImage->GetData());
    renderedImage->SavePNG("../../bin/Data/Textures/" + StringHash(model.name_).ToString() + "_albedo.png");

    //now switch to normal-only rendering shaders
    for(int i = 0; i < passes.Size(); i++)
    {
      String currentDefs = String(passes[i]->GetPixelShaderDefines());
      passes[i]->SetPixelShaderDefines(currentDefs + " NORMALGEN");
    }
    renderMod->SetMaterial(newMat);

    //SECOND RENDER
    rs->QueueUpdate();
  }
  else if(stage == IMPOSTER_SECOND)
  {
    std::cout << "Second" << std::endl;

    //revert shader
    for(int i = 0; i < passes.Size(); i++)
    {
      passes[i]->SetPixelShader(*oldShaders[i]);
    }
    renderMod->SetMaterial(newMat);

    //save image
    renderTexture->GetData(0, renderedImage->GetData());
    renderedImage->SavePNG("../../bin/Data/Textures/" + StringHash(model.name_).ToString() + "_normal.png");

    //make new material and assign
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    Texture2D* diffTex = cache->GetResource<Texture2D>("Textures/" + StringHash(model.name_).ToString() + "_albedo.png");
    Texture2D* normTex = cache->GetResource<Texture2D>("Textures/" + StringHash(model.name_).ToString() + "_normal.png");
    Material* defMat = cache->GetResource<Material>("Materials/BillDefault.xml");
    defMat->SetTexture(TU_DIFFUSE, diffTex);
    defMat->SetTexture(TU_NORMAL, normTex);
    //for the billboards
    SetMaterial(defMat);
    Commit();
  }
}

Vector2 Imposter::GetRenderPlaneSize(const BoundingBox& bb)
{
  float width = bb.max_.x_ - bb.min_.x_;
  //sqrt(pow(bb.max_.x_ - bb.min_.x_, 2) + pow(bb.max_.z_ - bb.min_.z_, 2));
  float height = bb.max_.y_ - bb.min_.y_;
  return Vector2(width, height);
}

void Imposter::SetImpModAttr(const ResourceRef& mod)
{
  model = mod;
}

void Imposter::SetImpMatAttr(const ResourceRef& mat)
{
  modelMat = mat;
}

ResourceRef Imposter::GetImpModAttr() const
{
  return model;
}

ResourceRef Imposter::GetImpMatAttr() const
{
  return modelMat;
}
