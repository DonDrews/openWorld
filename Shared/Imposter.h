#pragma once

#include <Urho3D/Core/Context.h>
#include <Urho3D/Graphics/BillboardSet.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Skybox.h>
#include <Urho3D/Graphics/Terrain.h>
#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Resource/Image.h>
#include <Urho3D/Graphics/TextureCube.h>
#include <Urho3D/Graphics/StaticModelGroup.h>
#include <Urho3D/Graphics/BillboardSet.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Technique.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Resource/XMLFile.h>
#include <Urho3D/Graphics/RenderPath.h>

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

class Imposter : public Urho3D::BillboardSet
{
  URHO3D_OBJECT(Imposter, Urho3D::BillboardSet);

private:

  static Urho3D::Vector2 GetRenderPlaneSize(const Urho3D::BoundingBox& bb);

  Urho3D::ResourceRef model;
  Urho3D::ResourceRef modelMat;

  Urho3D::RenderSurface* rs;
  Urho3D::StaticModel* renderMod;
  Urho3D::Material* newMat;
  Urho3D::Scene* renderScene;
  Urho3D::SharedPtr<Urho3D::Image> renderedImage;
  Urho3D::SharedPtr<Urho3D::Texture2D> renderTexture;
  Urho3D::PODVector<Urho3D::Pass*> passes;

  Urho3D::PODVector<Urho3D::String*> oldShaders;

  //constants
  static const unsigned CAM_AMOUNT = 8;

public:
  Imposter(Urho3D::Context* c);
  ~Imposter();

  enum Stage
  {
    IMPOSTER_SETUP,
    IMPOSTER_FIRST,
    IMPOSTER_SECOND
  };

  static void RegisterObject(Urho3D::Context* c);

  //called on client during scene load to generate correct textures
  void RenderTextures(Urho3D::Context* c, Stage s);

  void SetImpModAttr(const Urho3D::ResourceRef& mod);
  void SetImpMatAttr(const Urho3D::ResourceRef& mat);
  Urho3D::ResourceRef GetImpModAttr() const;
  Urho3D::ResourceRef GetImpMatAttr() const;
};
