#pragma once

#include <Urho3D/Core/Object.h>

//Event that clients send to the server right after connecting
//Contains information about the client
URHO3D_EVENT(E_LOGININFO, LoginInfo)
{
    URHO3D_PARAM(P_USERNAME, Username);                  // String
}
