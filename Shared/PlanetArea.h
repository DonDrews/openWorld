#pragma once

#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Skybox.h>
#include <Urho3D/Graphics/Terrain.h>
#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Resource/Image.h>
#include <Urho3D/Graphics/TextureCube.h>
#include <Urho3D/Graphics/StaticModelGroup.h>
#include <Urho3D/Graphics/BillboardSet.h>
#include <Urho3D/Network/Network.h>

#define _USE_MATH_DEFINES
#include <math.h>

#include "ProcSky.h"

class PlanetArea : public Urho3D::LogicComponent
{
  URHO3D_OBJECT(PlanetArea, LogicComponent);

public:
  //functions
  PlanetArea(Urho3D::Context* context);
  ~PlanetArea();

  //function to be called only once on the server side to make all components in the same node
  void GenComponents();
  Urho3D::Node* GenLocalContent(Urho3D::Context* c);

  static void RegisterObject(Urho3D::Context* context);

  void FixedUpdate(float timeStep);

  inline float GetTime(){return timeOfDay;};
  void SetTime(float t);

  //variables

  //time of day (sun position in radians)
  float timeOfDay;

private:

  //constants for day night cycle
  Color DARK_SUN;
  Color LIGHT_SUN;
  Color DARK_FOG;
  Color LIGHT_FOG;
  Color DARK_AMBIENT;
  Color LIGHT_AMBIENT;

  //cached component nodes (prevents search each update)
  SharedPtr<Urho3D::Node> zoneNode;
  SharedPtr<Urho3D::Node> sunNode;
  SharedPtr<Urho3D::Node> procNode;
};
