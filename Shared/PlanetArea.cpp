#include "PlanetArea.h"
#include "Imposter.h"
#include <iostream>

using namespace Urho3D;

PlanetArea::PlanetArea(Context* context) :
	LogicComponent(context),
	timeOfDay(1.2),
	DARK_SUN(0.0, 0.0, 0.0),
	LIGHT_SUN(8, 7.9, 7.5),
	DARK_FOG(0.0, 0.0, 0.0),
	LIGHT_FOG(2, 3, 4.5),
  DARK_AMBIENT(0.25, 0.25, 0.25, .5),
  LIGHT_AMBIENT(1, 1, 1, 5)
{
  //only fixed physics update
  SetUpdateEventMask(USE_FIXEDUPDATE);
}

PlanetArea::~PlanetArea()
{
  //Nothing here yet
}

void PlanetArea::RegisterObject(Context* context)
{
  std::cout << "Registering\n";
  context->RegisterFactory<PlanetArea>();

  //networked attributes
  URHO3D_ATTRIBUTE("Time", float, timeOfDay, 0.0f, AM_DEFAULT);
}

void PlanetArea::SetTime(float newTime)
{
    timeOfDay = newTime;
	procNode->GetComponent<ProcSky>()->timeOfDay_ = newTime;
    MarkNetworkUpdate();
}

Node* PlanetArea::GenLocalContent(Context* c)
{
  Node* thisNode = this->GetNode();
  // Create base node
  Node* staticLocalNode = thisNode->CreateChild("local", LOCAL);

  ResourceCache* cache = GetSubsystem<ResourceCache>();

  // Create heightmap terrain
  Node* terrainNode = staticLocalNode->CreateChild("Terrain", LOCAL);
  terrainNode->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
  Terrain* terrain = terrainNode->CreateComponent<Terrain>();
  terrain->SetPatchSize(64);
  terrain->SetOccluder(true);
  terrain->SetSpacing(Vector3(4.0f, 3.0f, 4.0f)); // Spacing between vertices and vertical resolution of the height map
  terrain->SetSmoothing(false);
  terrain->SetHeightMap(cache->GetResource<Image>("Textures/bigfinal.png"));
  terrain->SetMaterial(cache->GetResource<Material>("Materials/NewTerrain.xml"));
  terrain->SetCastShadows(true);

  // The terrain consists of large triangles, which fits well for occlusion rendering, as a hill can occlude all
  // terrain patches and other objects behind it
  //terrain->SetOccluder(true);
  //make physics component
  CollisionShape* terrainShape = terrainNode->CreateComponent<CollisionShape>();
  terrainShape->SetTerrain();
  RigidBody* terrainBody = terrainNode->CreateComponent<RigidBody>();
  terrainBody->SetMass(0.0f);
  terrainBody->SetCollisionLayerAndMask(128, 127);


  //vegetation test
  /*for(int i = 0; i < 250; i++)
  {
    Node* vegNode = staticLocalNode->CreateChild("Veg", LOCAL);
    //all grass is placed 10 meters below surface and stretched vertically to avoid being viewport culled (corrected in shader)
    Vector3 position(Random(2000.0f) - 1000.0f, 0.0f, Random(2000.0f) - 1000.0f);
    position.y_ = terrain->GetHeight(position) - 10.0f;
    vegNode->SetPosition(position);
    vegNode->SetScale(Vector3(10.0f, 100.0f, 10.0f));
    StaticModel* vegMod = vegNode->CreateComponent<StaticModel>();
    vegMod->SetModel(cache->GetResource<Model>("Models/Flower_Field.mdl"));
    vegMod->SetMaterial(1, cache->GetResource<Material>("Materials/GrassClump.xml"));
    vegMod->SetMaterial(0, cache->GetResource<Material>("Materials/FlowerClump.xml"));
    vegMod->SetDrawDistance(300.0f);
  }*/
  for (int i = 0; i < 8000; i++)
  {
	  Node* vegNode = staticLocalNode->CreateChild("Veg", LOCAL);
	  //all grass is placed 10 meters below surface and stretched vertically to avoid being viewport culled (corrected in shader)
	  Vector3 position(Random(8000.0f) - 4000.0f, 0.0f, Random(8000.0f) - 4000.0f);
	  position.y_ = terrain->GetHeight(position);
	  vegNode->SetPosition(position);
	  float x = Random(5.0f);
	  vegNode->SetScale(Vector3(10.0f + x, 100.0f + 5 * x, 10.0f + x));
	  StaticModel* vegMod = vegNode->CreateComponent<StaticModel>();
	  vegMod->SetModel(cache->GetResource<Model>("Models/Terrain_Foliage.mdl"));
	  vegMod->SetMaterial(0, cache->GetResource<Material>("Materials/GrassClump2.xml"));
	  vegMod->SetMaterial(1, cache->GetResource<Material>("Materials/GrassClump3.xml"));
	  vegMod->SetMaterial(2, cache->GetResource<Material>("Materials/GrassClump.xml"));
	  vegMod->SetDrawDistance(150.0f);
  }


  //tree Test

  Node* billNode = staticLocalNode->CreateChild("Billboards", LOCAL);
  Imposter* treeBills = billNode->CreateComponent<Imposter>();
  treeBills->SetNumBillboards(240000);
  treeBills->SetMaterial(cache->GetResource<Material>("Materials/BillDefault.xml"));
  treeBills->SetImpModAttr(ResourceRef(Model::GetTypeStatic(), "Models/pine.mdl"));
  treeBills->SetImpMatAttr(ResourceRef(Material::GetTypeStatic(), "Materials/Pine.xml"));
  treeBills->SetFaceCameraMode(FC_ROTATE_Y);

  int billCount = 0;

  for(int x = -4000; x < 4000; x += 200)
  {
    for(int y = -4000; y < 4000; y += 200)
    {

      //make group

      Node* treeGroupNode = staticLocalNode->CreateChild("TreeGroup", LOCAL);
      treeGroupNode->SetPosition(Vector3(float(x + 100), float(1250), float(y + 100)));

      StaticModelGroup* treeGroup = treeGroupNode->CreateComponent<StaticModelGroup>();
      treeGroup->SetModel(cache->GetResource<Model>("Models/pine.mdl"));
      treeGroup->SetMaterial(cache->GetResource<Material>("Materials/Pine.xml"));

      treeGroup->SetDrawDistance(500.0f);
      treeGroup->SetCastShadows(true);


      //make trees
      for(int i = 0; i < 150; i++)
      {
  
        Vector3 position(Random(200.0f) + x, 1250.0f, Random(200.0f) + y);
        position.y_ = terrain->GetHeight(position);

		if (position.y_ < terrain->GetHeight(Vector3(0.0f, 0.0f, 0.0f)) - 30.0f) {
			continue;
		}
		Node* vegNode = staticLocalNode->CreateChild("Tree", LOCAL);

        vegNode->SetPosition(position);
        float scale = Random(3.0f) + 5.0f;
        vegNode->SetScale(scale);
        float rotation = Random(360.0f);
        vegNode->SetRotation(Quaternion(rotation, Vector3(0.0f, 1.0f, 0.0f)));
        treeGroup->AddInstanceNode(vegNode);


        //set billboard properties
        Billboard* temp = treeBills->GetBillboard(billCount);
        BoundingBox bb = treeGroup->GetBoundingBox();
        temp->position_ = position;
        temp->position_.y_ += bb.max_.y_ * scale / 2;
        temp->size_ = Vector2(bb.max_.x_ * scale, bb.max_.y_ * scale / 2);
        //faked rotation matrix
        temp->color_ = Color(cos(-rotation * 2 * M_PI / 360.0f), sin(-rotation * 2 * M_PI / 360.0f), rotation / 360);
        temp->enabled_ = true;

        ++billCount;
      }
    }

  }

  treeBills->Commit();

  Node* watNode = staticLocalNode->CreateChild("water", LOCAL);
  Vector3 position(0.0f, 0.0f, 0.0f);
  position.y_ = terrain->GetHeight(position) - 30.0f;
  watNode->SetPosition(position);
  watNode->SetScale(10000);
  StaticModel* watMod = watNode->CreateComponent<StaticModel>();
  watMod->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
  watMod->SetMaterial(cache->GetResource<Material>("Materials/DeepWater.xml"));

  /*for(int x = -4000; x < 4000; x += 2000)
  {
    for(int y = -4000; y < 4000; y += 2000)
    {
      //for each rock model
      for(int i = 0; i < 4; i++)
      {
        Node* rockGroupNode = staticLocalNode->CreateChild("RockGroup", LOCAL);
        rockGroupNode->SetPosition(Vector3(float(x + 1000), float(1250), float(y + 1000)));

        StaticModelGroup* rockGroup = rockGroupNode->CreateComponent<StaticModelGroup>();
        Model* model = cache->GetResource<Model>("Models/Icosphere.00" + String(i + 1) + ".mdl");
        rockGroup->SetModel(model);
        rockGroup->SetMaterial(cache->GetResource<Material>("Materials/Rock_01.xml"));
        rockGroup->SetCastShadows(true);

        //for each model in group
        for(int j = 0; j < 128; j++)
        {
          Node* rockNode = staticLocalNode->CreateChild("Rock", LOCAL);

          Vector3 position(Random(2000.0f) + x, 0.0f, Random(2000.0f) + y);
      	  position.y_ = terrain->GetHeight(position);

          rockNode->SetPosition(position);
          rockNode->SetScale(Random(12.0f) + 3.0f);
      	  rockNode->SetRotation(Quaternion(Random(360.0f), Vector3(0.0f, 1.0f, 0.0f)));
          CollisionShape* rockShape = rockNode->CreateComponent<CollisionShape>();
          RigidBody* rockBody = rockNode->CreateComponent<RigidBody>();
          rockBody->SetMass(0.0f);
          rockBody->SetCollisionEventMode(COLLISION_NEVER);
          rockBody->SetCollisionLayerAndMask(128, 127);
          rockShape->SetConvexHull(model);

          rockGroup->AddInstanceNode(rockNode);
        }
      }
    }
  }*/

/*
 for (int x = -500; x < 500; x += 250)
  {
	  for (int y = -500; y < 500; y += 250)
	  {
		  //for each oak model
		  for (int i = 0; i < 4; i++)
		  {
			  Node* oakGroupNode = staticLocalNode->CreateChild("RockGroup", LOCAL);
			  oakGroupNode->SetPosition(Vector3(float(x + 1000), float(1250), float(y + 1000)));

			  StaticModelGroup* oakGroup = oakGroupNode->CreateComponent<StaticModelGroup>();
			  Model* model = cache->GetResource<Model>("Models/Tree" + String(i + 1) + ".mdl");
			  oakGroup->SetModel(model);
			  oakGroup->SetMaterial(0, cache->GetResource<Material>("Materials/Oak.xml"));
			  oakGroup->SetMaterial(1, cache->GetResource<Material>("Materials/Branch_Summer.xml"));
			  oakGroup->SetCastShadows(true);
			  oakGroup->SetDrawDistance(1000.0f);

			  //for each model in group
			  for (int j = 0; j < 32; j++)
			  {
				  Node* oakNode = staticLocalNode->CreateChild("oak", LOCAL);

				  Vector3 position(Random(2000.0f) + x, 0.0f, Random(2000.0f) + y);
				  position.y_ = terrain->GetHeight(position);

				  oakNode->SetPosition(position);
				  oakNode->SetScale(Random(0.5f) + 0.5f);
				  oakNode->SetRotation(Quaternion(Random(360.0f), Vector3(0.0f, 1.0f, 0.0f)));

				  oakGroup->AddInstanceNode(oakNode);
			  }
		  }
	  }
  }
  */

  return staticLocalNode;
}

void PlanetArea::GenComponents()
{
  context_->RegisterFactory<ProcSky>();

  Node* thisNode = this->GetNode();

  // Create scene content on the server only
  ResourceCache* cache = GetSubsystem<ResourceCache>();

  // All static scene content and the camera are also created as local, so that they are unaffected by scene replication and are
  // not removed from the client upon connection. Create a Zone component first for ambient lighting & fog control.
  zoneNode = thisNode->CreateChild("Zone");
  Zone* zone = zoneNode->CreateComponent<Zone>();
  zone->SetBoundingBox(BoundingBox(-5000.0f, 5000.0f));
  zone->SetAmbientColor(Color(1.0f, 1.0f, 1.0f, 5.0f));
  zone->SetFogColor(Color(2.0f, 3.0f, 4.5f));
  zone->SetFogStart(0.0f);
  zone->SetFogEnd(8000.0f);
  zone->SetZoneTexture(cache->GetResource<TextureCube>("Textures/Sky/Skybox2.xml"));

  // Create ProcSky required setup
  procNode = thisNode->CreateChild("ProcSky");
  ProcSky* proc = procNode->CreateComponent<ProcSky>();
  sunNode = procNode->CreateChild("DirectionalLight");
  sunNode->SetDirection(Vector3(0.5f, -1.0f, 0.8f));
  Light* sun = sunNode->CreateComponent<Light>();
  sun->SetLightType(LIGHT_DIRECTIONAL);
  sun->SetCastShadows(true);
  sun->SetShadowBias(BiasParameters(0.00025f, 0.5f));
  sun->SetShadowCascade(CascadeParameters(25.0f, 100.0f, 400.0f, 0.0f, 0.8f));
  sun->SetShadowIntensity(0.2f);
  sun->SetSpecularIntensity(0.2f);
  // Apply slightly overbright lighting to match the skybox
  sun->SetColor(Color(7.0f, 7.0f, 7.0f));
  //initialization happens on Client
}


void PlanetArea::FixedUpdate(float timeStep)
{
  if(!GetSubsystem<Network>()->GetServerConnection())
  {
    //use time of day to update sun, fog, and ambient color

    //sine of timeOfDay is used as a interpolation variable for all values
    //TODO change this to a more complex function in order to accurately represent atmospheric conditions

	timeOfDay += 0.0006;
    float interp = fmax(sin(timeOfDay), 0.05);

    Zone* zone = zoneNode->GetComponent<Zone>();
    Light* sun = sunNode->GetComponent<Light>();

    zone->SetAmbientColor(DARK_AMBIENT.Lerp(LIGHT_AMBIENT, interp));
    zone->SetFogColor(DARK_FOG.Lerp(LIGHT_FOG, interp));
    sun->SetColor(DARK_SUN.Lerp(LIGHT_SUN, interp));

    //sun direction is found by rotating around a gocal`round-parallel vector by timeOfDay
    Quaternion newRot((timeOfDay / M_PI) * 180, Vector3(1, 0, 1));
    sunNode->SetRotation(-newRot); //inverted since we are looking at direction from sun

    //Update procsky
    procNode->GetComponent<ProcSky>()->SetTime(timeOfDay);
  }
}
