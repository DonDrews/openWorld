//
// Copyright (c) 2008-2016 the Urho3D project.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Player.h"

using namespace Urho3D;

Player::Player(Context* context) :
    LogicComponent(context),
    onGround_(false),
    okToJump_(true),
    inAirTimer_(0.0f),
    name("Default")
{
    // Only the physics update event is needed: unsubscribe from the rest for optimization
    SetUpdateEventMask(USE_FIXEDUPDATE);
}

void Player::RegisterObject(Context* context)
{
    context->RegisterFactory<Player>();
    // These macros register the class attributes to the Context for automatic load / save handling.
    // We specify the Default attribute mode which means it will be used both for saving into file, and network replication
    URHO3D_ATTRIBUTE("On Ground", bool, onGround_, false, AM_DEFAULT);
    URHO3D_ATTRIBUTE("OK To Jump", bool, okToJump_, true, AM_DEFAULT);
    URHO3D_ATTRIBUTE("In Air Timer", float, inAirTimer_, 0.0f, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Name", String, name, String::EMPTY, AM_DEFAULT);
}

void Player::GenComponents()
{
    // Create scene content on the server only
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    // Component has been inserted into its scene node. Subscribe to events now
    SubscribeToEvent(GetNode(), E_NODECOLLISION, URHO3D_HANDLER(Player, HandleNodeCollision));

    Node* thisNode = this->node_;

    // Create the rendering component + animation controller
    AnimatedModel* object = thisNode->CreateComponent<AnimatedModel>();

    object->SetModel(cache->GetResource<Model>("Models/TTO_Char.mdl"));
    object->SetMaterial(0, cache->GetResource<Material>("Materials2/Cloth_2.xml"));
	object->SetMaterial(1, cache->GetResource<Material>("Materials2/Cloth_1.xml"));
	object->SetMaterial(2, cache->GetResource<Material>("Materials2/Leather_1.xml"));
	object->SetMaterial(3, cache->GetResource<Material>("Materials2/Rubber.xml"));
	object->SetMaterial(4, cache->GetResource<Material>("Materials2/Metal_1.xml"));
	object->SetMaterial(5, cache->GetResource<Material>("Materials2/Leather_2.xml"));
	object->SetMaterial(6, cache->GetResource<Material>("Materials2/Glass.xml"));
	object->SetMaterial(7, cache->GetResource<Material>("Materials2/Metal_2.xml"));
    object->SetCastShadows(true);
    thisNode->CreateComponent<AnimationController>();
    thisNode->SetScale(1.0f);

    /*Node* pistolNode = object->GetSkeleton().GetBone("hand.R")->node_->CreateChild("Pistol");

    AnimatedModel* pistol = pistolNode->CreateComponent<AnimatedModel>();
    Vector3 position(0.4675f, 1.7262f, -0.1693f);
    pistolNode->SetPosition(position);
    // Create a rotation quaternion from up vector to terrain normal
    pistolNode->SetRotation(Quaternion(-81.15f, 23.54f, -109.15f));
    pistol->SetModel(cache->GetResource<Model>("Models/Pistol.mdl"));
    pistol->SetMaterial(0, cache->GetResource<Material>("Materials/Brushed_Metal.xml"));
    pistol->SetMaterial(1, cache->GetResource<Material>("Materials/Gun_Rubber.xml"));
    pistol->SetMaterial(2, cache->GetResource<Material>("Materials/Gun_Metal.xml"));
    pistol->SetCastShadows(true);
    pistolNode->CreateComponent<AnimationController>();
    pistolNode->SetScale(0.25f);*/

    //Add a nametag to identify other players
    Node* tagNode = thisNode->CreateChild("NameTag");
    tagNode->SetPosition(Vector3(0.0, 2.3, -0.1));
    tagNode->SetScale(1.0);
    nametag = tagNode->CreateComponent<Text3D>();
    nametag->SetText(name);
    nametag->SetFont(cache->GetResource<Font>("Fonts/turnedtext.sdf"), 24);
    nametag->SetColor(Color::GREEN);
    nametag->SetTextEffect(TE_SHADOW);
    nametag->SetEffectColor(Color::GRAY);
    nametag->SetAlignment(HA_CENTER, VA_CENTER);
    nametag->SetFaceCameraMode(FC_ROTATE_Y);


    // Set the head bone for manual control
    //object->GetSkeleton().GetBone("neck")->animated_ = false;

    // Create rigidbody, and set non-zero mass so that the body becomes dynamic
    RigidBody* body = thisNode->CreateComponent<RigidBody>();
    body->SetCollisionLayer(1);
    body->SetMass(1.0f);

    // Set zero angular factor so that physics doesn't turn the character on its own.
    // Instead we will control the character yaw manually
    body->SetAngularFactor(Vector3::ZERO);

    // Set the rigidbody to signal collision also when in rest, so that we get ground collisions properly
    body->SetCollisionEventMode(COLLISION_ALWAYS);

    // Set a capsule shape for collision
    CollisionShape* shape = thisNode->CreateComponent<CollisionShape>();
    shape->SetCapsule(0.75f, 2.0f, Vector3(0.0f, 1.0f, 0.0f));
}

void Player::FixedUpdate(float timeStep)
{
  timePerPhys = timeStep;
}

void Player::applyControls(Controls controls)
{
  // Set rotation already here so that it's updated every rendering frame instead of every physics frame
  GetNode()->SetRotation(Quaternion(180 + controls.yaw_, Vector3::UP));

  /// \todo Could cache the components for faster access instead of finding them each frame
  RigidBody* body = GetComponent<RigidBody>();
  AnimationController* animCtrl = GetComponent<AnimationController>();

  // Update the in air timer. Reset if grounded
  if (!onGround_)
      inAirTimer_ += timePerPhys;
  else
      inAirTimer_ = 0.0f;
  // When character has been in air less than 1/10 second, it's still interpreted as being on ground
  bool softGrounded = inAirTimer_ < INAIR_THRESHOLD_TIME;

  // Update movement & animation
  const Quaternion& rot = node_->GetRotation();
  Vector3 moveDir = Vector3::ZERO;
  const Vector3& velocity = body->GetLinearVelocity();
  // Velocity on the XZ plane
  Vector3 planeVelocity(velocity.x_, 0.0f, velocity.z_);

  if (controls.buttons_ & CTRL_FORWARD)
      moveDir += Vector3::BACK;
  if (controls.buttons_ & CTRL_BACK)
      moveDir += Vector3::FORWARD;
  if (controls.buttons_ & CTRL_LEFT)
      moveDir += Vector3::RIGHT;
  if (controls.buttons_ & CTRL_RIGHT)
      moveDir += Vector3::LEFT;

  // Normalize move vector so that diagonal strafing is not faster
  if (moveDir.LengthSquared() > 0.0f)
      moveDir.Normalize();

  // If in air, allow control, but slower than when on ground
  body->ApplyImpulse(rot * moveDir * (softGrounded ? MOVE_FORCE : INAIR_MOVE_FORCE));

  if (softGrounded)
  {
      // When on ground, apply a braking force to limit maximum ground velocity
      Vector3 brakeForce = -planeVelocity * BRAKE_FORCE;
      body->ApplyImpulse(brakeForce);

      // Jump. Must release jump control inbetween jumps
      if (controls.buttons_ & CTRL_JUMP)
      {
          if (okToJump_)
          {
              body->ApplyImpulse(Vector3::UP * JUMP_FORCE);
              okToJump_ = false;
          }
      }
      else
          okToJump_ = true;
  }

  // Play animation if moving on ground, otherwise fade it out
	if (softGrounded && !moveDir.Equals(Vector3::ZERO))
	{
		animCtrl->PlayExclusive("Animations/Walk.ani", 0, true, 0.2f);
		animCtrl->SetSpeed("Animations/Walk.ani", planeVelocity.Length() * 0.5f);
		if (controls.buttons_ & CTRL_CROUCH)
		{
			body->ApplyImpulse(rot * moveDir *  MOVE_FORCE * -0.5);
			animCtrl->PlayExclusive("Animations/Crouch.ani", 0, true, 0.2f);
			animCtrl->SetSpeed("Animations/Crouch.ani", planeVelocity.Length() * 0.7f);
		}
		/*if (controls.buttons_ & CTRL_WIELD)
		{
			animCtrl->SetStartBone("Models/Space_Pistol2.ani", "spine");
			animCtrl->Play("Models/Space_Pistol2.ani", 0, true, 0.2f);
			animCtrl->SetSpeed("Models/Space_Pistol2.ani", planeVelocity.Length() *  0.1f);
		}*/
		else if (controls.buttons_ & CTRL_SPRINT)
		{
			body->ApplyImpulse(rot * moveDir *  MOVE_FORCE * 1.5f);
			animCtrl->PlayExclusive("Animations/Run.ani", 0, true, 0.2f);
			animCtrl->SetSpeed("Animations/Run.ani", planeVelocity.Length() *  0.3f);
		}
	}
	else
	{
		animCtrl->PlayExclusive("Animations/Idle.ani", 0, true, 0.2f);
		animCtrl->SetSpeed("Animations/Idle.ani", 0.5f);

		if (controls.buttons_ & CTRL_CROUCH)
		{
			body->ApplyImpulse(rot * moveDir *  MOVE_FORCE * -0.5f);
			animCtrl->PlayExclusive("Animations/Crouch.ani", 0, true, 0.2f);
			animCtrl->SetSpeed("Animations/Crouch.ani", planeVelocity.Length() * 0.5f);
		}
	}

	//jumping animation
	if (!softGrounded)
	{
		animCtrl->PlayExclusive("Animations/Jump.ani", 0, false, 0.2f);
		animCtrl->SetSpeed("Animations/Jump.ani", 0.7f);
	}
	/*if (controls.buttons_ & CTRL_WIELD)
	{
		animCtrl->SetStartBone("Models/Space_Pistol2.ani", "spine");
		animCtrl->Play("Models/Space_Pistol2.ani", 0, true, 0.2f);
		animCtrl->SetSpeed("Models/Space_Pistol2.ani", 0.1f);
	}*/

  // Reset grounded flag for next frame
  onGround_ = false;
}

void Player::HandleNodeCollision(StringHash eventType, VariantMap& eventData)
{
  // Check collision contacts and see if character is standing on ground (look for a contact that has near vertical normal)
  using namespace NodeCollision;

  MemoryBuffer contacts(eventData[P_CONTACTS].GetBuffer());

  while (!contacts.IsEof())
  {
      Vector3 contactPosition = contacts.ReadVector3();
      Vector3 contactNormal = contacts.ReadVector3();
      /*float contactDistance = */contacts.ReadFloat();
      /*float contactImpulse = */contacts.ReadFloat();

      // If contact is below node center and mostly vertical, assume it's a ground contact
      if (contactPosition.y_ < (node_->GetPosition().y_ + 1.0f))
      {
          float level = Abs(contactNormal.y_);
          if (level > 0.75)
              onGround_ = true;
      }
  }
}

void Player::SetName(const String& s)
{
  name = String(s);
  //adjust Text3D
  nametag->SetText(name);
  MarkNetworkUpdate();
}
